<?php

namespace Drupal\graphicsmagick;

use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the trait for configurable effects.
 */
trait ConfigurableEffectTrait {

  use EffectTrait;

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the effect plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    foreach ($this->defaultConfiguration as $key => $value) {
      $this->configuration[$key] = $form_state->getValue($key);
    }
  }

}
