<?php

namespace Drupal\graphicsmagick\Attribute;

/**
 * Defines an EffectDefaultConfiguration attribute object.
 */
#[\Attribute(\Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class EffectDefaultConfiguration {

  // phpcs:disable Drupal.Files.LineLength.TooLong

  /**
   * Constructs a \Drupal\graphicsmagick\Attribute\EffectDefaultConfiguration attribute.
   *
   * @param string $id
   *   The configuration value ID.
   * @param mixed $value
   *   The configuration value.
   */
  public function __construct(
    public string $id,
    public mixed $value,
  ) {}

}
