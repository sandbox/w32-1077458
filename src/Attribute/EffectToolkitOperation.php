<?php

namespace Drupal\graphicsmagick\Attribute;

/**
 * Defines an EffectToolkitOperation attribute object.
 */
#[\Attribute(\Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class EffectToolkitOperation {

  // phpcs:disable Drupal.Files.LineLength.TooLong

  /**
   * Constructs a \Drupal\graphicsmagick\Attribute\EffectToolkitOperation attribute.
   *
   * @param string $operation
   *   The operation name.
   * @param array $arguments
   *   The operation arguments.
   */
  public function __construct(
    public string $operation,
    public array $arguments = [],
  ) {}

}
