<?php

namespace Drupal\graphicsmagick;

/**
 * Interface for the GraphicsMagick toolkit.
 */
interface GraphicsMagickToolkitInterface {

  /**
   * Gets the maximum quantum value supported by Gmagick.
   *
   * @return int
   *   The maximum quantum value supported by Gmagick.
   */
  public function getQuantumRange(): int;

  /**
   * Gets the \Gmagick instance that handles the images.
   *
   * @return \Gmagick
   *   The \Gmagick instance used to handle the images.
   */
  public function getImageHandler(): \Gmagick;

  /**
   * Sets the \Gmagick instance that handles the images.
   *
   * @param \Gmagick $new_handler
   *   The \Gmagick instance used to handle the images.
   *
   * @return $this
   */
  public function setImageHandler(\Gmagick $new_handler): self;

  /**
   * Returns the array containing the default values for image properties.
   *
   * @return array
   *   The array containing the default values for the image properties, for
   *   example its width, its height, or its format.
   */
  public function getDefaultImageInfo(): array;

  /**
   * Returns the information about the image, such as width and height.
   *
   * @return array
   *   The array containing the information about the image, for example its
   *   width and height.
   */
  public function getImageInfo(): array;

  /**
   * Retrieves the image extension from the format internally used from Gmagick.
   *
   * @param string $format
   *   The image format, as returned by Gmagick::getImageFormat().
   *
   * @return string
   *   The image extension, or an empty string if $format is not recognized by
   *   the Gmagick extension.
   */
  public function extensionFromImageFormat(string $format): string;

  /**
   * Retrieves the image MIME type from the format internally used from Gmagick.
   *
   * @param string $format
   *   The image format, as returned by Gmagick::getImageFormat().
   *
   * @return string
   *   The MIME type, or an empty string if $format is not recognized by the
   *   Gmagick extension.
   */
  public function mimeTypeFromImageFormat(string $format): string;

  /**
   * Retrieves the format internally used from Gmagick from the image extension.
   *
   * @param string $extension
   *   The image extension.
   *
   * @return string
   *   The format as returned by Gmagick::getImageFormat(), or an empty string
   *   if $extension is not recognized by the Gmagick extension.
   */
  public function imageFormatFromExtension(string $extension): string;

}
