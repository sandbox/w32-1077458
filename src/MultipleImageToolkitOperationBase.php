<?php

namespace Drupal\graphicsmagick;

use Drupal\Core\ImageToolkit\ImageToolkitOperationBase;

/**
 * Base class for operations applied to each image in an image container.
 */
abstract class MultipleImageToolkitOperationBase extends ImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments): bool {
    $image_handler = $this->getToolkit()->getImageHandler();

    try {
      // Clone the image handler and work with the clone. In case of exceptions,
      // the original image handler and its state is preserved.
      $handler = clone $image_handler;

      if ($handler->getNumberImages() > 1) {
        $handler = $handler->coalesceImages();

        do {
          $this->runOperation($handler, $arguments);
        } while ($handler->nextImage());

        $handler = $handler->deconstructImages();
      }
      else {
        $this->runOperation($handler, $arguments);
      }
    }
    catch (\GmagickException | \Error) {
      try {
        $handler->destroy();
      }
      catch (\GmagickException) {
        // Gmagick::destroy() can throw an exception, but nothing can be done to
        // fix that.
      }

      return FALSE;
    }

    $this->getToolkit()->setImageHandler($handler);

    return TRUE;
  }

  /**
   * Applies the operation to the image.
   *
   * @param \Gmagick $handler
   *   The \Gmagick instance containing the images to change.
   * @param array $arguments
   *   An associative array of arguments to be used by the toolkit operation.
   *
   * @throws \GmagickException
   *   An error happened when running the operation.
   * @throws \Error
   *   The Gmagick extension is not loaded.
   */
  abstract protected function runOperation(\Gmagick &$handler, array $arguments): void;

}
