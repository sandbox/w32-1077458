<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick solarize operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_solarize",
  toolkit: "graphicsmagick",
  operation: "solarize",
  label: new TranslatableMarkup("Solarize"),
  description: new TranslatableMarkup("Simulates the effect achieved in a photo darkroom by selectively exposing areas of photo sensitive paper to light.")
)]
class Solarize extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'threshold' => [
        'description' => 'The extent of the solarization.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Convert the threshold to an integer.
    $arguments['threshold'] = (int) round($arguments['threshold']);

    $range = $this->getToolkit()->getQuantumRange();

    if ($arguments['threshold'] < 0 || $arguments['threshold'] >= $range) {
      throw new \InvalidArgumentException("Invalid threshold ('{$arguments['threshold']}') specified for the image 'solarize' operation");
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->solarizeImage($arguments['threshold']);
  }

}
