<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick scale and crop operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_scale_and_crop",
  toolkit: "graphicsmagick",
  operation: "scale_and_crop",
  label: new TranslatableMarkup("Scale and crop"),
  description: new TranslatableMarkup("Scales an image to the exact width and height given. It achieves the target aspect ratio by cropping the original image equally on both sides, or equally on the top and bottom.")
)]
class ScaleAndCrop extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'x' => [
        'description' => 'The horizontal offset for the start of the crop, in pixels.',
        'required' => FALSE,
        'default' => NULL,
      ],
      'y' => [
        'description' => 'The vertical offset for the start the crop, in pixels.',
        'required' => FALSE,
        'default' => NULL,
      ],
      'width' => [
        'description' => 'The target width, in pixels.',
      ],
      'height' => [
        'description' => 'The target height, in pixels.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    $actual_width = $this->getToolkit()->getWidth();
    $actual_height = $this->getToolkit()->getHeight();

    $scale_factor = max($arguments['width'] / $actual_width, $arguments['height'] / $actual_height);

    $arguments['x'] = isset($arguments['x']) ?
      (int) round($arguments['x']) :
      (int) round(($actual_width * $scale_factor - $arguments['width']) / 2);
    $arguments['y'] = isset($arguments['y']) ?
      (int) round($arguments['y']) :
      (int) round(($actual_height * $scale_factor - $arguments['height']) / 2);
    $arguments['resize'] = [
      'width' => (int) round($actual_width * $scale_factor),
      'height' => (int) round($actual_height * $scale_factor),
    ];

    // Verify width and height are greater than 0.
    if ($arguments['width'] <= 0) {
      throw new \InvalidArgumentException("Invalid width ('{$arguments['width']}') specified for the image 'scale_and_crop' operation");
    }
    if ($arguments['height'] <= 0) {
      throw new \InvalidArgumentException("Invalid height ('{$arguments['height']}') specified for the image 'scale_and_crop' operation");
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->resizeImage(
      $arguments['resize']['width'],
      $arguments['resize']['height'],
      // Image::scaleAndCrop() does not pass values for the following two
      // parameters.
      \Gmagick::FILTER_UNDEFINED,
      0.88
    );
    $handler = $handler->cropImage(
      $arguments['width'],
      $arguments['height'],
      $arguments['x'],
      $arguments['y']
    );
  }

}
