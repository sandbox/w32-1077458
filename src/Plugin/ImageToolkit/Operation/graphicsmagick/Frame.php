<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;
use Drupal\graphicsmagick\Utility\Color;

/**
 * Defines the GraphicsMagick frame operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_frame",
  toolkit: "graphicsmagick",
  operation: "frame",
  label: new TranslatableMarkup("Frame"),
  description: new TranslatableMarkup("Adds a simulated three-dimensional border.")
)]
class Frame extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'matte' => [
        'description' => "The border matte color.",
      ],
      'width' => [
        'description' => "The border width.",
      ],
      'height' => [
        'description' => "The border height.",
      ],
      'inner_bevel' => [
        'description' => "The inner bevel width.",
      ],
      'outer_bevel' => [
        'description' => "The outer bevel width.",
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    if (!Color::validate($arguments['matte'])) {
      throw new \InvalidArgumentException("Invalid color ({$arguments['matte']}) specified for the image 'frame' operation");
    }

    $matte = Color::toRgb($arguments['matte']) + ['a' => 0];
    $arguments['matte'] = Color::toHex($matte, TRUE);

    foreach (['width', 'height', 'inner_bevel', 'outer_bevel'] as $key) {
      // Convert the arguments to integers.
      $arguments[$key] = (int) round($arguments[$key]);

      // Verify they are not negative.
      if ($arguments[$key] < 0) {
        throw new \InvalidArgumentException("Invalid $key ('{$arguments[$key]}') specified for the image 'frame' operation");
      }
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->frameImage(
      $arguments['matte'],
      $arguments['width'],
      $arguments['height'],
      $arguments['inner_bevel'],
      $arguments['outer_bevel']
    );
  }

}
