<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick blur operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_blur",
  toolkit: "graphicsmagick",
  operation: "blur",
  label: new TranslatableMarkup("Blur"),
  description: new TranslatableMarkup("Makes the image details less distinct by applying a Gaussian operator with the given radius and standard deviation (sigma).")
)]
class Blur extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'radius' => [
        'description' => 'The Gaussian radius value.',
      ],
      'sigma' => [
        'description' => 'The Gaussian standard deviation value.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Convert all arguments to floating point.
    foreach (['radius', 'sigma'] as $key) {
      $arguments[$key] = (float) filter_var($arguments[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

      // Verify they are positive.
      if ($arguments[$key] <= 0.0) {
        throw new \InvalidArgumentException("Invalid $key ('$arguments[$key]') specified for the image 'blur' operation");
      }
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->blurImage($arguments['radius'], $arguments['sigma']);
  }

}
