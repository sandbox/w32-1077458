<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\SingleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick convert operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_convert",
  toolkit: "graphicsmagick",
  operation: "convert",
  label: new TranslatableMarkup("Convert"),
  description: new TranslatableMarkup("Convert the image to a different format.")
)]
class Convert extends SingleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'extension' => [
        'description' => 'The new extension of the converted image.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    if (!in_array($arguments['extension'], $this->getToolkit()->getSupportedExtensions())) {
      throw new \InvalidArgumentException("Invalid extension ({$arguments['extension']}) specified for the image 'convert' operation");
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $toolkit = $this->getToolkit();

    $handler->setImageFormat($toolkit->imageFormatFromExtension($arguments['extension']));
  }

}
