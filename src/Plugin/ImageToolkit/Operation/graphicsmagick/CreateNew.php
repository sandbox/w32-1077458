<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\SingleImageToolkitOperationBase;
use Drupal\graphicsmagick\Utility\Color;

/**
 * Defines the GraphicsMagick create_new operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_create_new",
  toolkit: "graphicsmagick",
  operation: "create_new",
  label: new TranslatableMarkup("Set a new image"),
  description: new TranslatableMarkup("Creates a new image.")
)]
class CreateNew extends SingleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'width' => [
        'description' => 'The width of the image, in pixels.',
      ],
      'height' => [
        'description' => 'The height of the image, in pixels.',
      ],
      'extension' => [
        'description' => 'The extension of the image file (for example, png, gif, or jpg).',
        'required' => FALSE,
        'default' => 'png',
      ],
      'transparent_color' => [
        'description' => 'The transparent color.',
        'required' => FALSE,
        'default' => 'transparent',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Verify width and height are greater than zero.
    $arguments['width'] = (int) round($arguments['width']);
    $arguments['height'] = (int) round($arguments['height']);
    if ($arguments['width'] <= 0) {
      throw new \InvalidArgumentException("Invalid width ('{$arguments['width']}') specified for the image 'create_new' operation");
    }
    if ($arguments['height'] <= 0) {
      throw new \InvalidArgumentException("Invalid height ({$arguments['height']}) specified for the image 'create_new' operation");
    }

    // Verify the extension is supported.
    if (!in_array($arguments['extension'], $this->getToolkit()->getSupportedExtensions())) {
      throw new \InvalidArgumentException("Invalid extension ('{$arguments['extension']}') specified for the image 'create_new' operation");
    }

    if (!Color::validate($arguments['transparent_color'])) {
      throw new \InvalidArgumentException("Invalid color ({$arguments['transparent_color']}) specified for the image 'create_new' operation");
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = new \Gmagick();
    $toolkit = $this->getToolkit();

    $handler->newImage(
      $arguments['width'],
      $arguments['height'],
      $arguments['transparent_color'],
      $toolkit->imageFormatFromExtension($arguments['extension'])
    );
  }

}
