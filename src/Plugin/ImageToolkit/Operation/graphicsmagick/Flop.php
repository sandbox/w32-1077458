<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick flop operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_flop",
  toolkit: "graphicsmagick",
  operation: "flop",
  label: new TranslatableMarkup("Flop"),
  description: new TranslatableMarkup("Creates a horizontal mirror image."),
)]
class Flop extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->flopImage();
  }

}
