<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick spread operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_spread",
  toolkit: "graphicsmagick",
  operation: "spread",
  label: new TranslatableMarkup("Spread"),
  description: new TranslatableMarkup("Randomly displaces each pixel in a circular region.")
)]
class Spread extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'radius' => [
        'description' => 'The radius of the circular region used by the operation.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Convert the radius to a floating point.
    $arguments['radius'] = (float) filter_var($arguments['radius'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

    // Verify it is greater than zero.
    if ($arguments['radius'] <= 0.0) {
      throw new \InvalidArgumentException("Invalid radius ('{$arguments['radius']}') specified for the image 'spread' operation");
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->spreadImage($arguments['radius']);
  }

}
