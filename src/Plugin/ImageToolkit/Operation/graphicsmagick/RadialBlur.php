<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick radial_blur operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_radial_blur",
  toolkit: "graphicsmagick",
  operation: "radial_blur",
  label: new TranslatableMarkup("Radial blur"),
  description: new TranslatableMarkup("Simulates a radial blur.")
)]
class RadialBlur extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'angle' => [
        'description' => 'The blur angle in degrees.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Convert the angle to floating point.
    $arguments['angle'] = (float) filter_var($arguments['angle'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

    // Convert it to a positive angle between 0 and 360 degrees.
    $arguments['angle'] -= floor($arguments['angle'] / 360) * 360;

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->radialBlurImage($arguments['angle']);
  }

}
