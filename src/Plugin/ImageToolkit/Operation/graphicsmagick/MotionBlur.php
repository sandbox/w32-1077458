<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick motion_blur operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_motion_blur",
  toolkit: "graphicsmagick",
  operation: "motion_blur",
  label: new TranslatableMarkup("Motion blur"),
  description: new TranslatableMarkup("Simulates a motion blur by applying a Gaussian operator with the given radius and standard deviation (sigma).")
)]
class MotionBlur extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'radius' => [
        'description' => 'The Gaussian radius value.',
      ],
      'sigma' => [
        'description' => 'The Gaussian standard deviation value.',
      ],
      'angle' => [
        'description' => 'The angle of the motion blur in degrees.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Convert all arguments to floating point.
    foreach (['radius', 'sigma', 'angle'] as $key) {
      $arguments[$key] = (float) filter_var($arguments[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

      // Verify radius and sigma are greater than zero.
      if ($key !== 'angle' && $arguments[$key] <= 0.0) {
        throw new \InvalidArgumentException("Invalid $key ('{$arguments[$key]}') specified for the image 'motion_blur' operation");
      }
    }

    // Convert any negative angle to a positive one between 0 and 360 degrees.
    $arguments['angle'] -= floor($arguments['angle'] / 360) * 360;

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->motionBlurImage(
      $arguments['radius'],
      $arguments['sigma'],
      $arguments['angle']
    );
  }

}
