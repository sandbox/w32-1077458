<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick edge operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_edge",
  toolkit: "graphicsmagick",
  operation: "edge",
  label: new TranslatableMarkup("Edge"),
  description: new TranslatableMarkup("Enhances the image edges.")
)]
class Edge extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'radius' => [
        'description' => 'The radius of the circle changed by the operation.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Convert the radius to a floating point.
    $arguments['radius'] = (float) filter_var($arguments['radius'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

    // Verify it is not negative.
    if ($arguments['radius'] < 0.0) {
      throw new \InvalidArgumentException("Invalid radius ('{$arguments['radius']}') specified for the image 'edge' operation");
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->edgeImage($arguments['radius']);
  }

}
