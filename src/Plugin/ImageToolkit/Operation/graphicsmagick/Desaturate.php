<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick desaturate operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_desaturate",
  toolkit: "graphicsmagick",
  operation: "desaturate",
  label: new TranslatableMarkup("Desaturate"),
  description: new TranslatableMarkup("Converts an image to grayscale.")
)]
class Desaturate extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->setImageType(\Gmagick::IMGTYPE_GRAYSCALE);
  }

}
