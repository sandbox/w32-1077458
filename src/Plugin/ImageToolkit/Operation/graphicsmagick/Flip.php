<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick flip operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_flip",
  toolkit: "graphicsmagick",
  operation: "flip",
  label: new TranslatableMarkup("Flip"),
  description: new TranslatableMarkup("Creates a vertical mirror image.")
)]
class Flip extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->flipImage();
  }

}
