<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick swirl operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_swirl",
  toolkit: "graphicsmagick",
  operation: "swirl",
  label: new TranslatableMarkup("Swirl"),
  description: new TranslatableMarkup("Swirls the pixels about the center of the image."),
)]
class Swirl extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'degrees' => [
        'description' => 'The sweep of the arc through which each pixel is moved.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Convert any negative angle to a positive one between 0 and 360 degrees.
    $arguments['degrees'] -= floor($arguments['degrees'] / 360) * 360;

    return $arguments;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \GmagickException
   *   An error occurred during the operation.
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->swirlImage($arguments['degrees']);
  }

}
