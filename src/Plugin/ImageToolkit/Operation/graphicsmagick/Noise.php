<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick noise operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_noise",
  toolkit: "graphicsmagick",
  operation: "noise",
  label: new TranslatableMarkup("Noise"),
  description: new TranslatableMarkup("Adds random noise to the image.")
)]
class Noise extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'noise' => [
        'description' => 'The noise type.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Verify the noise is in the expected range.
    if ($arguments['noise'] < \Gmagick::NOISE_UNIFORM || $arguments['noise'] > \Gmagick::NOISE_POISSON) {
      throw new \InvalidArgumentException("Invalid noise ('{$arguments['noise']}') specified for the image 'noise' operation");
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->addNoiseImage($arguments['noise']);
  }

}
