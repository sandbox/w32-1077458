<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick bilevel operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_bilevel",
  toolkit: "graphicsmagick",
  operation: "bilevel",
  label: new TranslatableMarkup("Bilevel"),
  description: new TranslatableMarkup("Converts an image to a dithered, black-and-white image.")
)]
class BiLevel extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->setImageType(\Gmagick::IMGTYPE_BILEVEL);
  }

}
