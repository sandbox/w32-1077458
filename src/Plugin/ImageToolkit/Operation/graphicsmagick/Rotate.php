<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;
use Drupal\graphicsmagick\Utility\Color;

/**
 * Defines the GraphicsMagick rotate operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_rotate",
  toolkit: "graphicsmagick",
  operation: "rotate",
  label: new TranslatableMarkup("Rotate"),
  description: new TranslatableMarkup("Rotates an image by the given number of degrees.")
)]
class Rotate extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'degrees' => [
        'description' => 'The number of (clockwise) degrees to rotate the image.',
      ],
      'background' => [
        'description' => "A string specifying the color to use as background for the uncovered area of the image after the rotation.",
        'required' => FALSE,
        'default' => NULL,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Convert any negative angle to a positive one between 0 and 360 degrees.
    $arguments['degrees'] -= floor($arguments['degrees'] / 360) * 360;

    if (!empty($arguments['background'])) {
      if (!Color::validate($arguments['background'])) {
        throw new \InvalidArgumentException("Invalid color ({$arguments['background']}) specified for the image 'rotate' operation");
      }

      $background = Color::toRgb($arguments['background']) + ['a' => 0];
    }
    else {
      // The background color has not been specified; use transparent white as
      // background.
      $background = ['r' => 255, 'g' => 255, 'b' => 255, 'a' => 127];
    }

    $arguments['background'] = Color::toHex($background, TRUE);

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->rotateImage($arguments['background'], $arguments['degrees']);
  }

}
