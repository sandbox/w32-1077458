<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick charcoal operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_charcoal",
  toolkit: "graphicsmagick",
  operation: "charcoal",
  label: new TranslatableMarkup("Charcoal"),
  description: new TranslatableMarkup("Simulates a charcoal drawing."),
)]
class Charcoal extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'radius' => [
        'description' => 'The Gaussian radius value.',
      ],
      'sigma' => [
        'description' => 'The Gaussian standard deviation value.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    foreach (['radius', 'sigma'] as $key) {
      // Convert all arguments to floating point.
      $arguments[$key] = (float) filter_var($arguments[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

      // Verify they are not negative.
      if ($arguments[$key] <= 0.0) {
        throw new \InvalidArgumentException("Invalid $key ('$arguments[$key]') specified for the image 'charcoal' operation");
      }
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->charcoalImage($arguments['radius'], $arguments['sigma']);
  }

}
