<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick despeckle operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_despeckle",
  toolkit: "graphicsmagick",
  operation: "despeckle",
  label: new TranslatableMarkup("Despeckle"),
  description: new TranslatableMarkup("Reduces the speckle noise in an image while preserving the original image edges.")
)]
class Despeckle extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->despeckleImage();
  }

}
