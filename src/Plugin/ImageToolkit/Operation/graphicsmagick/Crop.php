<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick crop operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_crop",
  toolkit: "graphicsmagick",
  operation: "crop",
  label: new TranslatableMarkup("Crop"),
  description: new TranslatableMarkup("Crops an image to a rectangle specified by the given dimensions.")
)]
class Crop extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'x' => [
        'description' => 'The starting x offset at which to start the crop, in pixels.',
      ],
      'y' => [
        'description' => 'The starting y offset at which to start the crop, in pixels.',
      ],
      'width' => [
        'description' => 'The width of the cropped area, in pixels.',
        'required' => FALSE,
        'default' => NULL,
      ],
      'height' => [
        'description' => 'The height of the cropped area, in pixels.',
        'required' => FALSE,
        'default' => NULL,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Verify at least one dimension is given.
    if (empty($arguments['width']) && empty($arguments['height'])) {
      throw new \InvalidArgumentException("At least one dimension ('width' or 'height') must be provided to the image 'crop' operation");
    }

    // Preserve aspect.
    $aspect = $this->getToolkit()->getHeight() / $this->getToolkit()->getWidth();
    $arguments['height'] = empty($arguments['height']) ? $arguments['width'] * $aspect : $arguments['height'];
    $arguments['width'] = empty($arguments['width']) ? $arguments['height'] / $aspect : $arguments['width'];

    // Convert all arguments to integers.
    foreach (['x', 'y', 'width', 'height'] as $key) {
      $arguments[$key] = (int) round($arguments[$key]);

      // Verify the arguments are not negative.
      if ($arguments[$key] < 0) {
        throw new \InvalidArgumentException("Invalid $key ('$arguments[$key]') specified for the image 'crop' operation");
      }
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->cropImage(
      $arguments['width'],
      $arguments['height'],
      $arguments['x'],
      $arguments['y']
    );
  }

}
