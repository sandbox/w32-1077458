<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick equalize operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_equalize",
  toolkit: "graphicsmagick",
  operation: "equalize",
  label: new TranslatableMarkup("Equalize"),
  description: new TranslatableMarkup("Equalizes the image histogram.")
)]
class Equalize extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->equalizeImage();
  }

}
