<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick enhance operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_enhance",
  toolkit: "graphicsmagick",
  operation: "enhance",
  label: new TranslatableMarkup("Enhance"),
  description: new TranslatableMarkup("Improves the quality of a noisy image."),
)]
class Enhance extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->enhanceImage();
  }

}
