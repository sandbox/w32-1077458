<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick oil_paint operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_oil_paint",
  toolkit: "graphicsmagick",
  operation: "oil_paint",
  label: new TranslatableMarkup("Oil paint"),
  description: new TranslatableMarkup("Simulates an oil painting.")
)]
class OilPaint extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'radius' => [
        'description' => 'The radius of the circular region used by the operation.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Convert the radius to a floating point.
    $arguments['radius'] = (float) filter_var($arguments['radius'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

    // Verify it is greater than zero.
    if ($arguments['radius'] <= 0.0) {
      throw new \InvalidArgumentException("Invalid radius ('{$arguments['radius']}') specified for the image 'oil_paint' operation");
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->oilPaintImage($arguments['radius']);
  }

}
