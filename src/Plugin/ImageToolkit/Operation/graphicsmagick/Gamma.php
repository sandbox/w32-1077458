<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick gamma operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_gamma",
  toolkit: "graphicsmagick",
  operation: "gamma",
  label: new TranslatableMarkup("Gamma"),
  description: new TranslatableMarkup("Corrects the image gamma.")
)]
class Gamma extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'gamma' => [
        'description' => 'The image gamma.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Convert the gamma to a floating point.
    $arguments['gamma'] = (float) filter_var($arguments['gamma'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

    // Verify it is not negative.
    if ($arguments['gamma'] < 0.0) {
      throw new \InvalidArgumentException("Invalid gamma ('{$arguments['gamma']}') specified for the image 'gamma' operation");
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->gammaImage($arguments['gamma']);
  }

}
