<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick resize operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_resize",
  toolkit: "graphicsmagick",
  operation: "resize",
  label: new TranslatableMarkup("Resize"),
  description: new TranslatableMarkup("Resizes an image to the given dimensions (ignoring aspect ratio).")
)]
class Resize extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'width' => [
        'description' => 'The width of the resized image, in pixels.',
      ],
      'height' => [
        'description' => 'The height of the resized image, in pixels.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    foreach (['width', 'height'] as $key) {
      // Convert all arguments to integers.
      $arguments[$key] = (int) round($arguments[$key]);

      // Verify they are greater than 0.
      if ($arguments[$key] <= 0) {
        throw new \InvalidArgumentException("Invalid $key ('$arguments[$key]') specified for the image 'resize' operation");
      }
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->resizeImage(
      $arguments['width'],
      $arguments['height'],
      // Image::resize() does not pass values for the following two parameters.
      \Gmagick::FILTER_UNDEFINED,
      0.88
    );
  }

}
