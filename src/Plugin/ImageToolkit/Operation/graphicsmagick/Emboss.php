<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\MultipleImageToolkitOperationBase;

/**
 * Defines the GraphicsMagick emboss operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_emboss",
  toolkit: "graphicsmagick",
  operation: "emboss",
  label: new TranslatableMarkup("Emboss"),
  description: new TranslatableMarkup("Makes the image grayscale with a three-dimensional effect.")
)]
class Emboss extends MultipleImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'radius' => [
        'description' => 'The Gaussian radius value.',
      ],
      'sigma' => [
        'description' => 'The Gaussian standard deviation value.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Convert all arguments to floating point.
    foreach (['radius', 'sigma'] as $key) {
      $arguments[$key] = (float) filter_var($arguments[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    }

    // Verify radius is not negative.
    if ($arguments['radius'] < 0.0) {
      throw new \InvalidArgumentException("Invalid radius ('{$arguments['radius']}') specified for the image 'emboss' operation");
    }

    // Verify sigma is greater than zero.
    if ($arguments['sigma'] <= 0.0) {
      throw new \InvalidArgumentException("Invalid sigma ('{$arguments['sigma']}') specified for the image 'emboss' operation");
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    $handler = $handler->setImageType(\Gmagick::IMGTYPE_GRAYSCALE);
    $handler = $handler->embossImage($arguments['radius'], $arguments['sigma']);
  }

}
