<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit\Operation\graphicsmagick;

use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the GraphicsMagick scale operation.
 */
#[ImageToolkitOperation(
  id: "graphicsmagick_scale",
  toolkit: "graphicsmagick",
  operation: "scale",
  label: new TranslatableMarkup("Scale"),
  description: new TranslatableMarkup("Scales an image while maintaining aspect ratio. The resulting image can be smaller for one or both target dimensions.")
)]
class Scale extends Resize {

  /**
   * {@inheritdoc}
   */
  protected function arguments(): array {
    return [
      'width' => [
        'description' => 'The target width, in pixels. If this value is omitted, the scaling will based only on the height value.',
        'required' => FALSE,
        'default' => NULL,
      ],
      'height' => [
        'description' => 'The target height, in pixels. If this value is omitted, the scaling will based only on the width value.',
        'required' => FALSE,
        'default' => NULL,
      ],
      'upscale' => [
        'description' => 'A Boolean indicating that files smaller than the dimensions will be scaled up. This generally results in a low quality image.',
        'required' => FALSE,
        'default' => FALSE,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments): array {
    // Assure at least one dimension.
    if (empty($arguments['width']) && empty($arguments['height'])) {
      throw new \InvalidArgumentException("At least one dimension ('width' or 'height') must be provided to the image 'scale' operation");
    }

    // Calculate one of the dimensions from the other target dimension,
    // ensuring the same aspect ratio as the source dimensions. If one of the
    // target dimensions is missing, that is the one that is calculated. If both
    // are specified then the dimension calculated is the one that would not be
    // calculated to be bigger than its target.
    $aspect = $this->getToolkit()->getHeight() / $this->getToolkit()->getWidth();
    if (($arguments['width'] && !$arguments['height']) || ($arguments['width'] && $arguments['height'] && $aspect < $arguments['height'] / $arguments['width'])) {
      $arguments['height'] = (int) round($arguments['width'] * $aspect);
    }
    else {
      $arguments['width'] = (int) round($arguments['height'] / $aspect);
    }

    // Assure integers for all arguments.
    $arguments['width'] = (int) round($arguments['width']);
    $arguments['height'] = (int) round($arguments['height']);
    if ($arguments['width'] <= 0) {
      throw new \InvalidArgumentException("Invalid width ('{$arguments['width']}') specified for the image 'scale' operation");
    }
    if ($arguments['height'] <= 0) {
      throw new \InvalidArgumentException("Invalid height ('{$arguments['height']}') specified for the image 'scale' operation");
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function runOperation(\Gmagick &$handler, array $arguments): void {
    // Do not scale if the dimensions are not changed.
    $width = $this->getToolkit()->getWidth();
    $height = $this->getToolkit()->getHeight();
    if ($arguments['width'] !== $width || $arguments['height'] !== $height) {
      // Do not upscale if the option is not enabled.
      if ($arguments['upscale'] || ($arguments['width'] <= $width && $arguments['height'] <= $height)) {
        parent::runOperation($handler, $arguments);
      }
    }
  }

}
