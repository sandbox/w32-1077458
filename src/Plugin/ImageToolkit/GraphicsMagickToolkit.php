<?php

namespace Drupal\graphicsmagick\Plugin\ImageToolkit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\Exception\FileWriteException;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\ImageToolkit\Attribute\ImageToolkit;
use Drupal\Core\ImageToolkit\ImageToolkitBase;
use Drupal\Core\ImageToolkit\ImageToolkitOperationManagerInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\graphicsmagick\GraphicsMagickToolkitInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the GraphicsMagick toolkit for image manipulation.
 */
#[ImageToolkit(
  id: "graphicsmagick",
  title: new TranslatableMarkup("GraphicsMagick image manipulation toolkit"),
)]
class GraphicsMagickToolkit extends ImageToolkitBase implements GraphicsMagickToolkitInterface {

  use StringTranslationTrait;

  /**
   * The correspondence between the supported formats and the image extensions.
   *
   * The items in this array are ordered to leave the preferred format for an
   * extension after the other formats using the same extension. In this way,
   * PDF is the format returned for the pdf extension, and PNG is the format
   * returned for the png extension.
   */
  protected const EXTENSIONS = [
    'BMP' => 'bmp',
    'CIN' => 'cin',
    'DCX' => 'dcx',
    'DPX' => 'dpx',
    'EPI' => 'epi',
    'EPSF' => 'eps',
    'EPSI' => 'eps',
    'EPT' => 'eps',
    'EPT2' => 'eps',
    'EPT3' => 'eps',
    'EPS' => 'eps',
    'FITS' => 'fits',
    'GIF87' => 'gif',
    'GIF' => 'gif',
    'ICB' => 'icb',
    'JPEG' => 'jpeg',
    'JPG' => 'jpg',
    'PAM' => 'pam',
    'PBM' => 'pbm',
    'EPDF' => 'pdf',
    'PDF' => 'pdf',
    'PGM' => 'pgm',
    'PNG00' => 'png',
    'PNG24' => 'png',
    'PNG32' => 'png',
    'PNG48' => 'png',
    'PNG' => 'png',
    'PNM' => 'pnm',
    'PPM' => 'ppm',
    'PS' => 'ps',
    'SGI' => 'sgi',
    'TGA' => 'tga',
    'PTIF' => 'tif',
    'BIGTIFF' => 'tif',
    'TIFF' => 'tiff',
    'VDA' => 'vda',
    'VST' => 'vst',
    'WBMP' => 'wbmp',
    'WEBP' => 'webp',
    'XBM' => 'xbm',
    'PICON' => 'xpm',
  ];

  /**
   * The correspondence between the supported formats and the MIME types.
   */
  protected const MIME_TYPES = [
    'BIGTIFF' => 'image/tiff',
    'BMP' => 'image/x-ms-bmp',
    'CIN' => 'image/cineon',
    'DCX' => 'image/x-pc-paintbrush',
    'DPX' => 'image/x-dpx',
    'EPDF' => 'application/pdf',
    'EPI' => 'application/postscript',
    'EPS' => 'application/postscript',
    'EPSF' => 'application/postscript',
    'EPSI' => 'application/postscript',
    'EPT' => 'image/eps',
    'EPT2' => 'image/eps',
    'EPT3' => 'image/eps',
    'FITS' => 'image/fits',
    'GIF' => 'image/gif',
    'GIF87' => 'image/gif',
    'ICB' => 'image/x-tga',
    'JPEG' => 'image/jpeg',
    'JPG' => 'image/jpeg',
    'PAM' => 'image/x-portable-pixmap',
    'PBM' => 'image/x-portable-pixmap',
    'PDF' => 'application/pdf',
    'PGM' => 'image/x-portable-greymap',
    'PICON' => 'image/x-xpmi',
    'PNG' => 'image/png',
    'PNG00' => 'image/png',
    'PNG24' => 'image/png',
    'PNG32' => 'image/png',
    'PNG48' => 'image/png',
    'PNM' => 'image/x-portable-pixmap',
    'PPM' => 'image/x-portable-pixmap',
    'PS' => 'application/postscript',
    'PTIF' => 'image/tiff',
    'SGI' => 'imagine/sgi',
    'TGA' => 'image/x-tga',
    'TIFF' => 'image/tiff',
    'VDA' => 'image/x-tga',
    'VST' => 'image/x-tga',
    'WBMP' => 'image/vnd.wap.wbmp',
    'WEBP' => 'image/webp',
    'XBM' => 'image/x‑xbitmap',
  ];

  /**
   * The \Gmagick instance used to handle the images.
   *
   * @var \Gmagick|null
   */
  protected ?\Gmagick $imageHandler = NULL;

  /**
   * The information about the image.
   *
   * @var array
   */
  protected array $imageInfo = [];

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected StreamWrapperManagerInterface $streamWrapperManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  // phpcs:disable Drupal.Files.LineLength.TooLong

  /**
   * Constructs a new \Drupal\graphicsmagick\Plugin\ImageToolkit\GraphicsMagickToolkit object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\ImageToolkit\ImageToolkitOperationManagerInterface $operation_manager
   *   The toolkit operation manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The StreamWrapper manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation_manager
   *   The translation manager.
   */
  public function __construct(array $configuration, string $plugin_id, array $plugin_definition, ImageToolkitOperationManagerInterface $operation_manager, LoggerInterface $logger, ConfigFactoryInterface $config_factory, StreamWrapperManagerInterface $stream_wrapper_manager, FileSystemInterface $file_system, TranslationInterface $translation_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $operation_manager, $logger, $config_factory);
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->fileSystem = $file_system;
    $this->setStringTranslation($translation_manager);
  }

  /**
   * Destructs a \Drupal\graphicsmagick\Plugin\ImageToolkit\GraphicsMagickToolkit object.
   *
   * Frees memory associated with an image object.
   */
  public function __destruct() {
    $this->deleteImageHandler();
  }

  // phpcs:enable

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, mixed $plugin_definition): static {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('image.toolkit.operation.manager'),
      $container->get('logger.channel.image'),
      $container->get('config.factory'),
      $container->get('stream_wrapper_manager'),
      $container->get('file_system'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getRequirements(): array {
    $requirements = [];
    $requirements['version'] = [
      'title' => $this->t('Gmagick extension'),
    ];

    if (extension_loaded('gmagick') && class_exists('Gmagick')) {
      try {
        $gmagick = new \Gmagick();
        $version_array = $gmagick->getVersion();
        $version = $version_array['versionString'] ?? ($version_array['versionNumber'] ?? NULL);

        $requirements['version']['severity'] = REQUIREMENT_OK;
        $requirements['version']['value'] = (isset($version) ? $this->t('Version: %version', ['%version' => $version]) : $this->t('Version: unknown'));
        $requirements['version']['description'] = $this->t(
          'The <a href=":gmagick_link">Gmagick</a> extension has been enabled and loaded.',
          [':gmagick_link' => 'https://www.php.net/manual/en/book.gmagick.php']
        );
      }
      catch (\GmagickException | \Error) {
        $requirements['version']['severity'] = REQUIREMENT_ERROR;
        $requirements['version']['value'] = $this->t('Version: unknown');
        $requirements['version']['description'] = $this->t(
          'The <a href=":gmagick_link">Gmagick</a> extension has been loaded, but there has been an error when using the class it implements.',
          [':gmagick_link' => 'https://www.php.net/manual/en/book.gmagick.php']
        );

        return $requirements;
      }

      try {
        $handler = new \Gmagick();

        // Verify the undocumented methods used by the toolkit are implemented.
        $missing_methods = FALSE;
        $missing_methods |= !method_exists($handler, 'getNumberImages');
        $missing_methods |= !method_exists($handler, 'coalesceImages');
        $missing_methods |= !method_exists($handler, 'deconstructImages');

        if ($missing_methods) {
          $requirements['version']['severity'] = REQUIREMENT_ERROR;
          $requirements['version']['description'] = $this->t(
            'The <a href=":gmagick_link">Gmagick</a> extension has been loaded, but the <code>Gmagick</code> class it implements does not have the methods necessary to the gmagick toolkit.',
            [':gmagick_link' => 'https://www.php.net/manual/en/book.gmagick.php']
          );
        }

      }
      catch (\GmagickException | \Error) {
        // Handle the \Error exception in the case the Gmagick extension is not
        // loaded.
        $requirements['version']['severity'] = REQUIREMENT_ERROR;
        $requirements['version']['description'] = $this->t(
          'The <a href=":gmagick_link">Gmagick</a> extension has been loaded, but there has been an error when using the class it implements.',
          [':gmagick_link' => 'https://www.php.net/manual/en/book.gmagick.php']
        );
      }
    }
    else {
      if (!extension_loaded('gmagick')) {
        $requirements['version']['severity'] = REQUIREMENT_ERROR;
        $requirements['version']['value'] = $this->t('Not found');
        $requirements['version']['description'] = $this->t(
          'The <a href=":gmagick_link">Gmagick</a> extension is required. See the <a href=":gmagick_installation">installation</a> page for more information.',
          [
            ':gmagick_link' => 'https://www.php.net/manual/en/book.gmagick.php',
            ':gmagick_installation' => 'https://www.php.net/manual/en/gmagick.installation.php',
          ]
        );
      }
      elseif (!class_exists('Gmagick')) {
        // In the case the Gmagick extension has been loaded, but the Gmagick
        // class has not found, give a more specific error that explains that.
        $requirements['version']['severity'] = REQUIREMENT_ERROR;
        $requirements['version']['value'] = $this->t('Not found');
        $requirements['version']['description'] = $this->t(
          'The <a href=":gmagick_link">Gmagick</a> extension has been loaded, but the <code>Gmagick</code> class it should define has not been found.',
          [
            ':gmagick_link' => 'https://www.php.net/manual/en/book.gmagick.php',
          ]
        );
      }
    }

    return $requirements;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSupportedExtensions(): array {
    $supported_extensions = [];

    try {
      $handler = new \Gmagick();
      foreach ($handler->queryFormats() as $format) {
        if (isset(self::EXTENSIONS[$format])) {
          $supported_extensions[] = self::EXTENSIONS[$format];
        }
      }
    }
    catch (\GmagickException | \Error) {
      // Ignore the exception.
    }

    return array_unique($supported_extensions);
  }

  /**
   * {@inheritdoc}
   */
  public static function isAvailable(): bool {
    if (extension_loaded('gmagick') && class_exists('Gmagick')) {
      try {
        $handler = new \Gmagick();

        // Verify the undocumented methods used by the toolkit are implemented.
        $missing_methods = FALSE;
        $missing_methods |= !method_exists($handler, 'getNumberImages');
        $missing_methods |= !method_exists($handler, 'coalesceImages');
        $missing_methods |= !method_exists($handler, 'deconstructImages');

        return !$missing_methods;
      }
      catch (\GmagickException) {
        return FALSE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isValid(): bool {
    return $this->imageInfo || ($this->imageHandler instanceof \Gmagick);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['jpeg_image_quality'] = [
      '#type' => 'number',
      '#title' => $this->t('JPEG image quality'),
      '#description' => $this->t('Define the JPEG image quality (from 0 to 100). Higher values mean better image quality but bigger files.'),
      '#min' => 0,
      '#max' => 100,
      '#step' => 1,
      '#default_value' => $this->configFactory->getEditable('system.image.gmagick')->get('jpeg_image_quality'),
      '#field_suffix' => $this->t('%'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configFactory->getEditable('system.image.gmagick')
      ->set('jpeg_image_quality', $form_state->getValue(['gmagick', 'jpeg_image_quality']))
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultImageInfo(): array {
    return [
      'format' => '',
      'geometry' => ['width' => 0, 'height' => 0],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getImageInfo(): array {
    return !empty($this->imageInfo) ? $this->imageInfo : $this->getDefaultImageInfo();
  }

  /**
   * Sets the image handler and the image info array.
   *
   * @return $this
   */
  protected function createImageHandler(string $file): self {
    $default_image_info = $this->getDefaultImageInfo();
    $image_handler = new \Gmagick($file);
    try {
      $image_info = [
        'format' => $image_handler->getImageFormat(),
      ];

      if (method_exists($image_handler, 'getImageGeometry')) {
        $image_info['geometry'] = $image_handler->getImageGeometry();
      }
      else {
        $image_info['geometry'] = [
          'width' => $image_handler->getImageWidth(),
          'height' => $image_handler->getImageHeight(),
        ];
      }
    }
    catch (\GmagickException) {
      $image_info = $default_image_info;
    }

    $this->imageHandler = $image_handler;
    $this->imageInfo = $image_info;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuantumRange(): int {
    try {
      if ($this->imageHandler instanceof \Gmagick) {
        $handler = $this->imageHandler;
      }
      else {
        $handler = new \Gmagick();
      }

      $range_depth = $handler->getQuantumDepth();

      if (isset($range_depth['quantumDepthLong'])) {
        $range = 1 << $range_depth['quantumDepthLong'];
      }
      else {
        $range = 256;
      }

      return $range;
    }
    catch (\GmagickException | \Error) {
      return 256;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getImageHandler(): \Gmagick {
    return $this->imageHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function setImageHandler(\Gmagick $new_handler): self {
    if ($this->imageHandler instanceof \Gmagick) {
      try {
        $this->imageHandler->destroy();
      }
      catch (\GmagickException) {
        // Ignore exception.
      }
    }

    $this->imageHandler = $new_handler;

    return $this;
  }

  /**
   * Destroys the image handler and resets the image info.
   */
  protected function deleteImageHandler(): self {
    if ($this->imageHandler instanceof \Gmagick) {
      try {
        $this->imageHandler->destroy();
      }
      catch (\GmagickException) {
        // Ignore the exception.
      }
    }
    $this->imageHandler = NULL;
    $this->imageInfo = [];

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function parseFile(): bool {
    try {
      if (!$file = $this->localFile()) {
        $file = $this->copyToTemporary();
      }
      $this->createImageHandler($file);
      return TRUE;
    }
    catch (\Exception) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setSource($source): static {
    parent::setSource($source);
    $this->deleteImageHandler();

    return $this;
  }

  /**
   * Retrieves the absolute local file path.
   *
   * @param string $filename
   *   The file path.
   *
   * @return string
   *   The absolute file path.
   */
  protected function getRealPath(string $filename): string {
    if (($realpath = $this->fileSystem->realpath($filename)) === FALSE) {
      throw new FileException("The absolute local filepath for $filename cannot be determined.");
    }
    return $realpath;
  }

  /**
   * Gets the absolute local path.
   *
   * @param string|null $source
   *   The file for which the absolute local path is requested; if no argument
   *   is passed, the value returned from ImageToolkitBase::getSource() will be
   *   used.
   *
   * @return string
   *   The absolute local path, if the file is local, or an empty string.
   */
  protected function localFile(?string $source = NULL): string {
    if (!$source) {
      $source = $this->getSource();
    }

    $scheme = StreamWrapperManager::getScheme($source);
    if ($scheme && $this->streamWrapperManager->isValidScheme($scheme)) {
      $local_wrappers = $this->streamWrapperManager->getWrappers(StreamWrapperInterface::LOCAL);
      if (isset($local_wrappers[$scheme])) {
        return $this->getRealPath($source);
      }
    }

    return '';
  }

  /**
   * Copies the file in a temporary local file.
   *
   * @param string|null $source
   *   The file to copy; if no argument is passed, the value returned from
   *   ImageToolkitBase::getSource() will be used.
   *
   * @return string
   *   The absolute filepath of the temporary file.
   */
  protected function copyToTemporary(?string $source = NULL): string {
    if (($destination = $this->fileSystem->tempnam('temporary://', 'graphicsmagick_')) === FALSE) {
      throw new FileWriteException("The temporary file could not be created.");
    }
    // Copy the remote file to the temporary local file.
    $this->fileSystem->copy($source ?? $this->getSource(), $destination, FileExists::Replace);

    return $this->getRealPath($destination);
  }

  /**
   * Moves a file to a new location.
   *
   * @param string $source
   *   A string specifying the file path or URI of the source file.
   * @param string $destination
   *   A URI containing the destination where the source file should be moved
   *   to. The URI may be a bare file path (without a scheme) and in that case
   *   the default scheme (public://) will be used.
   */
  protected function moveFile(string $source, string $destination): void {
    $this->fileSystem->move($source, $destination, FileExists::Replace);
  }

  /**
   * {@inheritdoc}
   */
  public function save($destination): bool {
    try {
      if ($destination_path = $this->localFile($destination)) {
        $local_file = TRUE;
      }
      else {
        $destination_path = $this->copyToTemporary($destination);
      }

      $this->writeImage($destination_path);
    }
    catch (\Exception) {
      return FALSE;
    }

    if (!empty($local_file)) {
      return TRUE;
    }

    try {
      $this->moveFile($destination_path, $destination);
      return TRUE;
    }
    catch (FileException) {
      return FALSE;
    }
  }

  /**
   * Writes the image to a file.
   *
   * @param string $destination
   *   The destination file.
   */
  protected function writeImage(string $destination): self {
    if ($this->imageHandler instanceof \Gmagick) {
      try {
        if ($this->getMimeType() == 'image/jpeg') {
          $this->imageHandler->setCompressionQuality($this->getJpegImageQuality());
        }
        $this->imageHandler->writeImage($destination);
      }
      catch (\GmagickException) {
        // Ignore the exception.
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeight() {
    return (isset($this->imageInfo) ? $this->imageInfo['geometry']['height'] : NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function getWidth() {
    return (isset($this->imageInfo) ? $this->imageInfo['geometry']['width'] : NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function getMimeType(): string {
    $format = $this->getImageInfo()['format'];

    try {
      if (!$format && $this->imageHandler instanceof \Gmagick) {
        $format = $this->imageHandler->getImageFormat();
      }

      return $this->mimeTypeFromImageFormat($format);
    }
    catch (\GmagickException) {
      // Return an empty string to signal an error.
      return '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function extensionFromImageFormat(string $format): string {
    try {
      // Verify the passed format is one of the formats Gmagick::queryFormats()
      // would return.
      if ($this->imageHandler instanceof \Gmagick) {
        $handler = $this->imageHandler;
      }
      else {
        $handler = new \Gmagick();
      }

      $formats = $handler->queryFormats();

      if (in_array($format, $formats)) {
        return self::EXTENSIONS[$format] ?? '';
      }

      // Return an empty string to signal an error.
      return '';
    }
    catch (\GmagickException | \Error) {
      // Return an empty string to signal an error.
      return '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function mimeTypeFromImageFormat(string $format): string {
    // Since the Gmagick class doesn't provide a method to obtain an image MIME
    // type, differently from the Imagick class, this code maps the Gmagick
    // format to the MIME type using a literal array. There is no need to use
    // mime_content_type() nor finfo_open()/finfo_file(), since the Gmagick
    // class already recognized the file type.
    try {
      // Verify the passed format is one of the formats Gmagick::queryFormats()
      // would return.
      if ($this->imageHandler instanceof \Gmagick) {
        $handler = $this->imageHandler;
      }
      else {
        $handler = new \Gmagick();
      }

      $formats = $handler->queryFormats();

      if (in_array($format, $formats)) {
        return self::MIME_TYPES[$format] ?? '';
      }

      // Return an empty string to signal an error.
      return '';
    }
    catch (\GmagickException | \Error) {
      // Return an empty string to signal an error.
      return '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function imageFormatFromExtension(string $extension): string {
    $format = '';
    $supported_extensions = array_flip(self::EXTENSIONS);

    if (isset($supported_extensions[$extension])) {
      $format = $supported_extensions[$extension];

      try {
        // Verify the format is returned by Gmagick::queryFormats().
        if ($this->imageHandler instanceof \Gmagick) {
          $handler = $this->imageHandler;
        }
        else {
          $handler = new \Gmagick();
        }

        $formats = $handler->queryFormats();

        if (!in_array($format, $formats)) {
          $format = '';
        }
      }
      catch (\GmagickException | \Error) {
        // Return an empty string to signal an error.
        return '';
      }
    }

    return $format;
  }

  /**
   * Gets the image quality used for JPEG images.
   *
   * @return int
   *   The JPEG image quality, from 0 to 100.
   */
  protected function getJpegImageQuality(): int {
    return (int) $this->configFactory->getEditable('system.image.gmagick')->get('jpeg_image_quality');
  }

}
