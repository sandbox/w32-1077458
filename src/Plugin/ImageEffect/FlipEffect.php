<?php

namespace Drupal\graphicsmagick\Plugin\ImageEffect;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\Attribute\EffectToolkitOperation;
use Drupal\graphicsmagick\EffectTrait;
use Drupal\image\Attribute\ImageEffect;
use Drupal\image\ImageEffectBase;

/**
 * Defines the GraphicsMagick flip effect.
 */
#[ImageEffect(
  id: 'graphicsmagick_flip',
  label: new TranslatableMarkup('Flip'),
  description: new TranslatableMarkup('Creates a vertical mirror image.'),
)]
class FlipEffect extends ImageEffectBase {

  use EffectTrait;

  /**
   * Initializes the effect plugin.
   */
  #[EffectToolkitOperation('flip')]
  protected function initEffect(): void {}

}
