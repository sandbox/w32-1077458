<?php

namespace Drupal\graphicsmagick\Plugin\ImageEffect;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\Attribute\EffectToolkitOperation;
use Drupal\graphicsmagick\EffectTrait;
use Drupal\image\Attribute\ImageEffect;
use Drupal\image\ImageEffectBase;

/**
 * Defines the GraphicsMagick flop effect.
 */
#[ImageEffect(
  id: 'graphicsmagick_flop',
  label: new TranslatableMarkup('Flop'),
  description: new TranslatableMarkup('Creates a horizontal mirror image.'),
)]
class FlopEffect extends ImageEffectBase {

  use EffectTrait;

  /**
   * Initializes the effect plugin.
   */
  #[EffectToolkitOperation('flop')]
  protected function initEffect(): void {}

}
