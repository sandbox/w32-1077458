<?php

namespace Drupal\graphicsmagick\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\Attribute\EffectDefaultConfiguration;
use Drupal\graphicsmagick\Attribute\EffectToolkitOperation;
use Drupal\graphicsmagick\ConfigurableEffectTrait;
use Drupal\image\Attribute\ImageEffect;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * Defines the GraphicsMagick blur effect.
 */
#[ImageEffect(
  id: 'graphicsmagick_blur',
  label: new TranslatableMarkup('Blur'),
  description: new TranslatableMarkup('Makes the image details less distinct by applying a Gaussian operator.'),
)]
class BlurImageEffect extends ConfigurableImageEffectBase {

  use ConfigurableEffectTrait;

  /**
   * Initializes the effect plugin.
   */
  #[EffectDefaultConfiguration(id: 'radius', value: 0.0)]
  #[EffectDefaultConfiguration(id: 'sigma', value: 1.0)]
  #[EffectToolkitOperation('blur', arguments: ['$radius', '$sigma'])]
  protected function initEffect(): void {}

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['radius'] = [
      '#type' => 'number',
      '#title' => $this->t('Radius'),
      '#description' => $this->t('This value should typically be either 0, to let the effect choose the best value, or twice the sigma value.'),
      '#default_value' => $this->configuration['radius'],
      '#required' => TRUE,
      '#min' => 0.0,
      '#max' => 65355.0,
      '#step' => 0.5,
    ];
    $form['sigma'] = [
      '#type' => 'number',
      '#title' => $this->t('Sigma'),
      '#description' => $this->t('A large sigma value and a smallish radius value will introduce artifacts in the resulting image; small sigma values are typically only used to fuzz lines and smooth edges on images for which no anti-aliasing was used.'),
      '#default_value' => $this->configuration['sigma'],
      '#required' => TRUE,
      '#min' => 0.0,
      '#max' => 65355.0,
      '#step' => 0.5,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['radius'] = $form_state->getValue('radius');
    $this->configuration['sigma'] = $form_state->getValue('sigma');
  }

}
