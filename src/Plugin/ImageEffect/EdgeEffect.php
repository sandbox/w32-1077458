<?php

namespace Drupal\graphicsmagick\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\Attribute\EffectDefaultConfiguration;
use Drupal\graphicsmagick\Attribute\EffectToolkitOperation;
use Drupal\graphicsmagick\ConfigurableEffectTrait;
use Drupal\image\Attribute\ImageEffect;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * Defines the GraphicsMagick edge effect.
 */
#[ImageEffect(
  id: 'edge',
  label: new TranslatableMarkup('Edge'),
  description: new TranslatableMarkup('Enhances the image edges.'),
)]
class EdgeEffect extends ConfigurableImageEffectBase {

  use ConfigurableEffectTrait;

  /**
   * Initializes the effect plugin.
   */
  #[EffectDefaultConfiguration(id: 'radius', value: 0.0)]
  #[EffectToolkitOperation('edge', arguments: ['$radius'])]
  protected function initEffect(): void {}

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['radius'] = [
      '#type' => 'number',
      '#title' => $this->t('Radius'),
      '#description' => $this->t('Use a value of zero to let the effect choose the best value.'),
      '#default_value' => $this->configuration['radius'],
      '#required' => TRUE,
      '#min' => 0.0,
      '#max' => 65355.0,
      '#step' => 0.5,
    ];

    return $form;
  }

}
