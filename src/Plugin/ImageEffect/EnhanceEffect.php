<?php

namespace Drupal\graphicsmagick\Plugin\ImageEffect;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\Attribute\EffectToolkitOperation;
use Drupal\graphicsmagick\EffectTrait;
use Drupal\image\Attribute\ImageEffect;
use Drupal\image\ImageEffectBase;

/**
 * Defines the GraphicsMagick enhance effect.
 */
#[ImageEffect(
  id: 'graphicsmagick_enhance',
  label: new TranslatableMarkup('Enhance'),
  description: new TranslatableMarkup('Improves the quality of a noisy image.')
)]
class EnhanceEffect extends ImageEffectBase {

  use EffectTrait;

  /**
   * Initializes the effect plugin.
   */
  #[EffectToolkitOperation('enhance')]
  protected function initEffect(): void {}

}
