<?php

namespace Drupal\graphicsmagick\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\Attribute\EffectDefaultConfiguration;
use Drupal\graphicsmagick\Attribute\EffectToolkitOperation;
use Drupal\graphicsmagick\ConfigurableEffectTrait;
use Drupal\image\Attribute\ImageEffect;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * Defines the GraphicsMagick gamma effect.
 */
#[ImageEffect(
  id: 'graphicsmagick_gamma',
  label: new TranslatableMarkup('Gamma'),
  description: new TranslatableMarkup('Changes the gamma value.'),
)]
class GammaEffect extends ConfigurableImageEffectBase {

  use ConfigurableEffectTrait;

  /**
   * Initializes the effect plugin.
   */
  #[EffectDefaultConfiguration('gamma', value: 0.8)]
  #[EffectToolkitOperation('gamma', arguments: ['$gamma'])]
  protected function initEffect(): void {}

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['gamma'] = [
      '#type' => 'number',
      '#title' => $this->t('Gamma'),
      '#description' => $this->t('Values typically range from 0.8 to 2.3. Values lower than one make the image darker; values higher than one make the image lighter.'),
      '#default_value' => $this->configuration['gamma'],
      '#required' => TRUE,
      '#min' => 0.0,
      '#max' => 10.0,
      '#step' => 0.2,
    ];

    return $form;
  }

}
