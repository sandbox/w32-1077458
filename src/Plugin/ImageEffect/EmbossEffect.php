<?php

namespace Drupal\graphicsmagick\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\Attribute\EffectDefaultConfiguration;
use Drupal\graphicsmagick\Attribute\EffectToolkitOperation;
use Drupal\graphicsmagick\ConfigurableEffectTrait;
use Drupal\image\Attribute\ImageEffect;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * Defines the GraphicsMagick emboss effect.
 */
#[ImageEffect(
  id: 'graphicsmagick_emboss',
  label: new TranslatableMarkup('Emboss'),
  description: new TranslatableMarkup('Gives the image a three-dimensional effect.'),
)]
class EmbossEffect extends ConfigurableImageEffectBase {

  use ConfigurableEffectTrait;

  /**
   * Initializes the effect plugin.
   */
  #[EffectDefaultConfiguration(id: 'radius', value: 0.0)]
  #[EffectDefaultConfiguration(id: 'sigma', value: 1.0)]
  #[EffectToolkitOperation('emboss', arguments: ['$radius', '$sigma'])]
  protected function initEffect(): void {}

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['radius'] = [
      '#type' => 'number',
      '#title' => $this->t('Radius'),
      '#description' => $this->t('For reasonable results, the radius value should be higher than the sigma value. Use a value of zero to let the effect choose the best value.'),
      '#default_value' => $this->configuration['radius'],
      '#required' => TRUE,
      '#min' => 0.0,
      '#max' => 65355.0,
      '#step' => 0.5,
    ];
    $form['sigma'] = [
      '#type' => 'number',
      '#title' => $this->t('Sigma'),
      '#description' => $this->t('A large sigma value works well also with color image. It can remove color dominance, but it could also introduce artifacts, especially in JPEG images.'),
      '#default_value' => $this->configuration['sigma'],
      '#required' => TRUE,
      '#min' => 0.0,
      '#max' => 65355.0,
      '#step' => 0.5,
    ];

    return $form;
  }

}
