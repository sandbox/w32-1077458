<?php

namespace Drupal\graphicsmagick\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\Attribute\EffectDefaultConfiguration;
use Drupal\graphicsmagick\Attribute\EffectToolkitOperation;
use Drupal\graphicsmagick\ConfigurableEffectTrait;
use Drupal\image\Attribute\ImageEffect;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * Defines the GraphicsMagick charcoal effect.
 */
#[ImageEffect(
  id: 'graphicsmagick_charcoal',
  label: new TranslatableMarkup('Charcoal'),
  description: new TranslatableMarkup('Simulates a charcoal drawing.')
)]
class CharcoalImageEffect extends ConfigurableImageEffectBase {

  use ConfigurableEffectTrait;

  /**
   * Initializes the effect plugin.
   */
  #[EffectDefaultConfiguration(id: 'radius', value: 0.0)]
  #[EffectDefaultConfiguration(id: 'sigma', value: 1.0)]
  #[EffectToolkitOperation('charcoal', arguments: ['$radius', '$sigma'])]
  protected function initEffect(): void {}

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['radius'] = [
      '#type' => 'number',
      '#title' => $this->t('Radius'),
      '#description' => $this->t('The radius of the Gaussian, in pixels, not counting the center pixel.'),
      '#default_value' => $this->configuration['radius'],
      '#required' => TRUE,
      '#min' => 0.0,
      '#max' => 65355.0,
      '#step' => 0.5,
    ];
    $form['sigma'] = [
      '#type' => 'number',
      '#title' => $this->t('Sigma'),
      '#description' => $this->t('The standard deviation of the Gaussian, in pixels.'),
      '#default_value' => $this->configuration['sigma'],
      '#required' => TRUE,
      '#min' => 0.0,
      '#max' => 65355.0,
      '#step' => 0.5,
    ];

    return $form;
  }

}
