<?php

namespace Drupal\graphicsmagick\Plugin\ImageEffect;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\Attribute\EffectToolkitOperation;
use Drupal\graphicsmagick\EffectTrait;
use Drupal\image\Attribute\ImageEffect;
use Drupal\image\ImageEffectBase;

/**
 * Defines the GraphicsMagick equalize effect.
 */
#[ImageEffect(
  id: 'graphicsmagick_equalize',
  label: new TranslatableMarkup('Equalize'),
  description: new TranslatableMarkup('Equalizes the image histogram.'),
)]
class EqualizeEffect extends ImageEffectBase {

  use EffectTrait;

  /**
   * Initializes the effect plugin.
   */
  #[EffectToolkitOperation('equalize')]
  protected function initEffect(): void {}

}
