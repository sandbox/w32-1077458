<?php

namespace Drupal\graphicsmagick\Plugin\ImageEffect;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\Attribute\EffectToolkitOperation;
use Drupal\graphicsmagick\EffectTrait;
use Drupal\image\Attribute\ImageEffect;
use Drupal\image\ImageEffectBase;

/**
 * Defines the GraphicsMagick despeckle effect.
 */
#[ImageEffect(
  id: 'graphicsmagick_despeckle',
  label: new TranslatableMarkup('Despeckle'),
  description: new TranslatableMarkup('Despeckle the image.')
)]
class DespeckleEffect extends ImageEffectBase {

  use EffectTrait;

  /**
   * Initializes the effect plugin.
   */
  #[EffectToolkitOperation('despeckle')]
  protected function initEffect(): void {}

}
