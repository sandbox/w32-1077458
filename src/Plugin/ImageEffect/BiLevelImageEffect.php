<?php

namespace Drupal\graphicsmagick\Plugin\ImageEffect;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphicsmagick\Attribute\EffectToolkitOperation;
use Drupal\graphicsmagick\EffectTrait;
use Drupal\image\Attribute\ImageEffect;
use Drupal\image\ImageEffectBase;

/**
 * Defines the GraphicsMagick bilevel effect.
 */
#[ImageEffect(
  id: 'graphicsmagick_bilevel',
  label: new TranslatableMarkup('Bilevel'),
  description: new TranslatableMarkup('Converts an image to a dithered, black-and-white image.'),
)]
class BiLevelImageEffect extends ImageEffectBase {

  use EffectTrait;

  /**
   * Initializes the effect plugin.
   */
  #[EffectToolkitOperation('bilevel')]
  protected function initEffect(): void {}

}
