<?php

namespace Drupal\graphicsmagick\Exception;

/**
 * Exception thrown when a required method is not found.
 */
class MissingMethodException extends \Exception {}
