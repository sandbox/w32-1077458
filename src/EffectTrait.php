<?php

namespace Drupal\graphicsmagick;

use Drupal\Core\Image\ImageInterface;
use Drupal\graphicsmagick\Attribute\EffectDefaultConfiguration;
use Drupal\graphicsmagick\Attribute\EffectToolkitOperation;
use Drupal\graphicsmagick\Exception\MissingMethodException;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\AutowiringFailedException;

/**
 * Defines the trait for image effect plugins.
 */
trait EffectTrait {

  /**
   * The default configuration values.
   *
   * @var array
   */
  protected array $defaultConfiguration = [];

  /**
   * The image toolkit operations applied by the effect.
   *
   * @var array
   */
  protected array $effectOperations = [];

  /**
   * Instantiates a new image effect plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   * @param array $configuration
   *   The configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *
   * @throws \Drupal\graphicsmagick\Exception\MissingMethodException
   *   A required method is not implemented from the class.
   * @throws \Symfony\Component\DependencyInjection\Exception\AutowiringFailedException
   *    One of the services used by initEffect() could not be autowired.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $args = [];
    $service = NULL;

    $object = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $object->setStringTranslation($container->get('string_translation'));

    if (!method_exists(static::class, 'initEffect')) {
      throw new MissingMethodException(sprintf('%s::initEffect() is missing', static::class));
    }

    $init_effect = new \ReflectionMethod(static::class, 'initEffect');

    foreach ($init_effect->getAttributes(EffectDefaultConfiguration::class) as $attribute) {
      $instance = $attribute->newInstance();
      $object->defaultConfiguration[$instance->id] = $instance->value;
    }

    foreach ($init_effect->getAttributes(EffectToolkitOperation::class) as $attribute) {
      $instance = $attribute->newInstance();
      $object->effectOperations[$instance->operation] = $instance->args;
    }

    foreach ($init_effect->getParameters() as $parameter) {
      foreach ($parameter->getAttributes(Autowire::class) as $attribute) {
        $service = (string) $attribute->newInstance()->value;
      }

      if (!isset($service)) {
        $service = ltrim((string) $parameter->getType(), '?');
      }

      if (!$container->has($service)) {
        throw new AutowiringFailedException($service, sprintf('Cannot autowire service "%s" (argument "$%s" of "%s::initEffect()")', $service, $parameter->getName(), static::class));
      }

      $args[] = $container->get($service);
    }

    $object->initEffect(...$args);

    return $object;
  }

  /**
   * Gets the default configuration for the effect plugin.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  public function defaultConfiguration(): array {
    return $this->defaultConfiguration;
  }

  /**
   * Applies an image effect to the image object.
   *
   * @param \Drupal\Core\Image\ImageInterface $image
   *   The image object.
   *
   * @return bool
   *   TRUE on success, FALSE if unable to perform the image effect on the
   *   image.
   */
  public function applyEffect(ImageInterface $image): bool {
    $result = TRUE;

    foreach ($this->effectOperations as $operation => $args) {
      if (isset($this->configuration)) {
        foreach ($args as &$arg) {
          if (is_string($arg) && $arg[0] == '$') {
            $config_name = substr($arg, 1);

            if (isset($this->configuration[$config_name])) {
              $arg = $this->configuration[$config_name];
            }
          }
        }
      }

      if (!$result = $image->apply($operation, $args)) {
        break;
      }
    }

    return $result;
  }

}
